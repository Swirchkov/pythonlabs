from tasks import task1, task2, task3, task5
from tasks.task1 import FibbonacciIterator
from tasks.task4 import Sealed
from random import randint
import time

# task 1
print('-------------------- task1 ---------------------------')

N = 15
print('fibbonacci numbers till {0}'.format(N))
iterator = FibbonacciIterator(N)

for i in iterator:
    print(i)

# task 2
print('-------------------- task2 -----------------------')

@task2.async_decorator
def test_func():
    #time.sleep(2)
    print('test function.')


test_func()
#time.sleep(3)

# task 3

print("---------------------- task3 ------------------------")

test_lst = [[1, 2, 4], '', 'qwerty', []]

print('printing items length from')
print(test_lst)
task3.print_list_items_length(test_lst)

# task 4

print("---------------------- task4 ------------------------")

print('expecting error when creating divided class from sealed class ')


# class Bar(Sealed):
#     pass


# task 5
print("---------------------- task5 ------------------------")

test_passes = ["acd44", "aqsdsadqadqwd", "1234567890", "acd45wefwefwf44"]

print('testing check password')

for index, test_str in enumerate(test_passes):
    print('testing "{0}" and got - {1}'.format(test_str, task5.check_password(test_str)))

