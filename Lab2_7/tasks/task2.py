import inspect
from multiprocessing import Pool
from threading import Thread

def async_decorator(func):

    def wrapper():
        print('started async execution')
        thread = Thread(target=func)
        thread.start()

    return wrapper


# def print_wrapper(func):
#     def wrapper(*args, **kwargs):
#         print(' name - ' + func.__name__)
#
#         args_definition = inspect.getfullargspec(func)
#         print('Declared definition - {0}'.format(args_definition))
#
#         if len(args) > 0:
#             print('passed parameters')
#             for index, value in enumerate(args):
#                 print('parameter {0} - {1}'.format(index, value))
#
#         if len(kwargs) > 0:
#             print('passed parametrized parameters')
#             for key, value in enumerate(kwargs):
#                 print('{0} - {1}'.format(key, value))
#
#         func(*args, **kwargs)
#     return wrapper

