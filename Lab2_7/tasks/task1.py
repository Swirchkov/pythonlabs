class FibbonacciIterator:

    limit = 0
    __current = 0

    __prev1 = 0
    __prev2 = 0

    def __iter__(self):
        return self

    def __init__(self, limit):
        self.limit = limit

    def __next__(self):
        if self.__current == self.limit:
            raise StopIteration

        result = 0
        if self.__prev1 == 0:
            self.__prev1 = 1
            result = self.__prev1

        elif self.__prev2 == 0:
            self.__prev2 = 1
            result = self.__prev2

        else:
            result = self.__prev1 + self.__prev2
            self.__prev1, self.__prev2 = result, self.__prev1

        self.__current += 1
        return result

