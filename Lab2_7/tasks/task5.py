import re


def check_password(str):
    letters_in_password = r"[a-zA-Z]{1}"
    digits_in_password = r"\d{1}"

    return len(str) > 8 and len(re.findall(letters_in_password, str)) > 0 \
           and len(re.findall(digits_in_password, str)) > 0

