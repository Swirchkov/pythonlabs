from math import sin, pi


def angle_to_sin(angles):
    return list(map(lambda angle: (angle, sin(angle * (pi / 180))), angles))

