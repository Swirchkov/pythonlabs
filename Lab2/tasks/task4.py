
class Student:
    marks = []

    def add_mark(self, mark):
        self.marks.append(mark)

    def primary_average(self):
        return self.__average(True)

    def additional_average(self):
        return self.__average(False)

    def general_average(self):
        selected_marks = list(map(lambda m: m.mark, self.marks))
        return sum(selected_marks) / len(selected_marks)

    def __average(self, is_primary):
        filtered_marks = list(filter(lambda m: m.is_primary == is_primary, self.marks))
        selected_marks = list(map(lambda m: m.mark, filtered_marks))
        return sum(selected_marks) / len(selected_marks)


class Mark:
    mark = 0
    subject_name = ''
    is_primary = True

    def __init__(self, mark, subject_name, is_primary):
        self.is_primary = is_primary
        self.mark = mark
        self.subject_name = subject_name