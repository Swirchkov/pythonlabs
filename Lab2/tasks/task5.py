import re


def replace_spaces(str):
    return re.sub(r"\s+", process_match, str)


def process_match(match):
    match_str = match.group()
    return match_str if match_str.startswith('\n') else " "
