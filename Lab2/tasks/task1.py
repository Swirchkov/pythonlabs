def filter_paired(lst):
    return [lst[x] for x in range(0, len(lst), 1) if lst[x] % 2 == 0]
