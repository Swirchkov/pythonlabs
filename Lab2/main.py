from tasks import task1, task2, task3, task5
from tasks.task4 import Student, Mark
from random import randint
import time

# task 1
print('-------------------- task1 ---------------------------')
size = 30
src = [randint(size, size * 2) for t1 in range(0, size, 1)]
print('source list')
print(src)
print('filtered list')
print(task1.filter_paired(src))

# task 2
print('-------------------- task2 -----------------------')

@task2.async_decorator
def test_func():
    time.sleep(2)
    print('test function.')


test_func()
time.sleep(3)

# task 3

print("---------------------- task3 ------------------------")

angles = [t3 for t3 in range(0, 91, 1)]

print('generating relation for')
print(angles)
print('generated')
print(task3.angle_to_sin(angles))


# task 4

print("---------------------- task4 ------------------------")

student = Student()

student.add_mark(Mark(80, "name1", True))
student.add_mark(Mark(85, "name1", True))
student.add_mark(Mark(95, "name1", True))
student.add_mark(Mark(90, "name1", True))

student.add_mark(Mark(90, "name2", False))
student.add_mark(Mark(98, "name2", False))

print('average primary marks - {0}'.format(student.primary_average()))
print('average additional marks - {0}'.format(student.additional_average()))
print('general average mark - {0}'.format(student.general_average()))

# task 5
print("---------------------- task5 ------------------------")

test_str = "   asdasd asasdads          asdas" \
           "\n   asxcscewfwef       awdwqqwddew    win"

print('testing string')
print(test_str)

print('replaced string')
print(task5.replace_spaces(test_str))
