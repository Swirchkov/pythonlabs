def translate_array(lst):
    return [lst[x] if x % 2 == 1 else lst[x + 1] if x + 1 < len(lst) else lst[x] for x in range(0, len(lst), 1)]