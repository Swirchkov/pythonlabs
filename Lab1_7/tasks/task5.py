alphabet = "ABCDEFGHIKLMNOPQRSTVXYZ"


def print_alphabet():
    i = 0
    while i < len(alphabet):
        end = (i + 6) if (i + 6) < len(alphabet) else len(alphabet)
        print(alphabet[i : end])
        i = end
