
def sum_array(lst):
    arr = [lst[x] for x in range(0, len(lst), 1) if lst[x] < 11 and lst[x] % 2 == 1]
    return sum(arr)
