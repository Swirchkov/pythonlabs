
def generate_paired_sequence(end):
    return [x for x in range(2, end, 2)]