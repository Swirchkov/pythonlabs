import re


def count_re_matches(regex, str):
    matches = re.findall(regex, str)
    return len(matches)

def print_matches(str):
    print("'+' - {0}".format(count_re_matches(r'\+', str)))
    print("'-' - {0}".format(count_re_matches(r'-', str)))
    print("'+ or - with 0' - {0}".format(count_re_matches(r'[\+-]0', str)))