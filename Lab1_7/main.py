from tasks import task1, task3, task4, task5, task6, task7, task8, task9, task10
from random import randint

# task 1
print('!!!!!!!!!!!!!!!!!!!!!!! task1 !!!!!!!!!!!!!!!!!!!!')
task1.print_task()

# task 3
print('!!!!!!!!!!!!!!!!!!!!!!! task3 !!!!!!!!!!!!!!!!!!!!')

print('updating 78')
print(task3.update_num(78))

print('updating number 7')
print(task3.update_num(7))

print('updating number -33')
print(task3.update_num(-33))

# task 4
print('!!!!!!!!!!!!!!!!!!!!!!! task4 !!!!!!!!!!!!!!!!!!!!')

print('printin sequence')
task4.print_sequence()

# task 5
print('!!!!!!!!!!!!!!!!!!!!!!! task5 !!!!!!!!!!!!!!!!!!!!')

print('ALPHABET')
task5.print_alphabet()

# task 6
print('!!!!!!!!!!!!!!!!!!!!!!! task6 !!!!!!!!!!!!!!!!!!!!')

print('generated sequence till 90')
print(task6.generate_paired_sequence(90))

# task 7
print('!!!!!!!!!!!!!!!!!!!!!!! task7 !!!!!!!!!!!!!!!!!!!!')

test = [randint(1, 20) for x in range(1, 10, 1)]
print('counting for')
print(test)
print('received')
print(task7.sum_array(test))

# task 8
print('!!!!!!!!!!!!!!!!!!!!!!! task8 !!!!!!!!!!!!!!!!!!!!')

test = [randint(10, 30) for x in range(1, 10, 1)]
print('modifying array')
print(test)
print('received ')
print(task8.translate_array(test))

# task 9
print('!!!!!!!!!!!!!!!!!!!!!!! task9 !!!!!!!!!!!!!!!!!!!!')

size = 5
matrix = [[randint(size, size**2) for v in range(0, size, 1)] for k in range(0, size, 1)]
print('source matrix {0}'.format(len(matrix)))
print(matrix)
print('diagonal')
task9.print_diagonal(matrix)

# task 10
print('!!!!!!!!!!!!!!!!!!!!!!! task10 !!!!!!!!!!!!!!!!!!!!')

test_str = "+++ sfsdf + -- -0 +0 - -+0"

print('counting matches in "{0}"'.format(test_str))
task10.print_matches(test_str)