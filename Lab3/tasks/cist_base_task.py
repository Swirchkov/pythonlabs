from abc import ABC, abstractmethod
import urllib.request, json


class CistBaseTask(ABC):

    root_url = "http://cist.nure.ua/ias/app/tt/"

    def open_json_by_url(self, url):
        with urllib.request.urlopen(url) as response:
            data = json.loads(response.read().decode('cp1251'))
            return data

