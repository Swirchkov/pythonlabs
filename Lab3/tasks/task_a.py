from tasks.cist_base_task import CistBaseTask
from json.decoder import JSONDecodeError

class TaskA(CistBaseTask):

    def __init__(self):
        self.faculty_url = self.root_url + "P_API_FACULTIES_JSON"
        self.departments_url = self.root_url + "P_API_DEPARTMENTS_JSON?p_id_faculty={0}"

    def process_cist_request(self):
        json_data = self.open_json_by_url(self.faculty_url)

        for faculty in json_data['university']['faculties']:
            self.process_faculty(faculty)

    def process_faculty(self, faculty):
        print("Факультет - '" + faculty['full_name'] + "'")

        try:
            departments = self.open_json_by_url(self.departments_url.format(faculty['id']))

            for department in departments['faculty']['departments']:
                print("\t" + department['full_name'])
        except JSONDecodeError:
            print('\treceived invalid json')

