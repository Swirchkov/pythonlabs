from tasks.cist_base_task import CistBaseTask
from utils.time_util import Time
from datetime import datetime
from utils.schedule_helpers import ScheduleHelper

from json.decoder import JSONDecodeError
from urllib.error import HTTPError

class Department:
    def __init__(self):
        self.department = ''
        self.teachers = set()

class TaskD(CistBaseTask):

    def __init__(self):
        self.university_structure = self.root_url + "P_API_PODR_JSON"
        self.teacher_events = self.root_url + "P_API_EVENTS_TEACHER_JSON?p_id_teacher={0}&time_from={1}&time_to={2}"

    def process_lections_schedule(self):
        structure = self.open_json_by_url(self.university_structure)

        departments = list()
        for faculty in structure['university']['faculties']:
            for department in faculty['departments']:

                dep = Department()
                dep.department = department['full_name']

                for teacher in department['teachers']:
                    dep.teachers.add(teacher['id'])

                departments.append(dep)

        today = Time.get_today_bounds()
        for department in departments:
            print('Lections by department {}'.format(department.department))

            for teacher_id in department.teachers:
                url = self.teacher_events.format(teacher_id, int(today[0]), int(today[1]))
                try:
                    schedule = self.open_json_by_url(url)
                    helpers = ScheduleHelper(schedule)

                    for event in schedule['events']:
                        if int(event['type']) < 10:
                            start = datetime.fromtimestamp(event['start_time'])
                            end = datetime.fromtimestamp(event['end_time'])
                            subject_title = helpers.get_subject_title(event['subject_id'])

                            print('{:%Y-%m-%d} from {:%H:%M} to {:%H:%M} subject - {}'.format(start, start, end,
                                                                                          subject_title))
                        pass
                    pass
                except JSONDecodeError:
                    print("\t received invalid json by url - " + url)
                except HTTPError:
                    print('\t server failed to response on - ' + url)
            pass
        pass
    pass

