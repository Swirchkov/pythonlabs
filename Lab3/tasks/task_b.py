from tasks.cist_base_task import CistBaseTask
from json.decoder import JSONDecodeError
from datetime import datetime
from utils.schedule_helpers import ScheduleHelper

class TaskB(CistBaseTask):

    def __init__(self):
        self.group_id = 4801920
        self.schedule_by_group = self.root_url + "P_API_EVENTS_GROUP_JSON?p_id_group={0}&time_from={1}&time_to={2}"

    def parse_schedule(self, time_from, time_to):
        url = self.schedule_by_group.format(self.group_id, int(time_from), int(time_to))
        try:
            schedule = self.open_json_by_url(url)
            helpers = ScheduleHelper(schedule)
            for event in schedule['events']:

                start = datetime.fromtimestamp(event['start_time'])
                end = datetime.fromtimestamp(event['end_time'])
                subject_title = helpers.get_subject_title(event['subject_id'])

                print('----------------------------------------------------------------')

                print('{:%Y-%m-%d} from {:%H:%M} to {:%H:%M} subject - {}'.format(start, start, end, subject_title))
                print('teachers')
                for teacher_id in event['teachers']:
                    print('\t' + helpers.get_teacher(teacher_id))
                print('event type - {}'.format(helpers.get_event_type(event['type'])))

        except JSONDecodeError:
            print('\treceived invalid json by next url - ' + url)
