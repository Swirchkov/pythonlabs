from tasks.cist_base_task import CistBaseTask
from utils.schedule_helpers import ScheduleHelper
from utils.time_util import Time
from json.decoder import JSONDecodeError

class TaskC(CistBaseTask):

    def __init__(self):
        self.url = self.root_url + "P_API_EVENTS_AUDITORY_JSON?p_id_auditory={0}&time_from={1}&time_to={2}"

    def parse_auditory_schedule(self, auditory_id):
        today_bounds = Time.get_today_bounds()
        url = self.url.format(str(auditory_id), int(today_bounds[0]), int(today_bounds[1]))
        try :
            schedule = self.open_json_by_url(url)
            groups = set()
            teachers = set()

            for event in schedule['events']:
                for group_id in event['groups']:
                    if group_id not in groups:
                        groups.add(group_id)

                for teacher_id in event['teachers']:
                    if teacher_id not in teachers:
                        teachers.add(teacher_id)

            helper = ScheduleHelper(schedule)

            print ('groups in auditory today')
            for group_id in groups:
                print("\t {0}".format(helper.get_group_name(group_id)))

            print ('teachers in auditory today')
            for teacher_id in teachers:
                print("\t {0}".format(helper.get_teacher(teacher_id)))

        except JSONDecodeError:
            print('\treceived invalid json by next url - ' + url)