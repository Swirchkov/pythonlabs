
class ScheduleHelper:

    def __init__(self, schedule):
        self.schedule = schedule

    def get_subject_title(self, subject_id):
        for subject in self.schedule['subjects']:
            if subject['id'] == subject_id:
                return subject['title']

        return ''

    def get_teacher(self, teacher_id):
        for teacher in self.schedule['teachers']:
            if teacher['id'] == teacher_id:
                return teacher['full_name']

        return ''

    def get_group_name(self, group_id):
        for group in self.schedule['groups']:
            if group['id'] == group_id:
                return group['name']

        return ''

    def get_event_type(self, type_id):
        for type in self.schedule['types']:
            if type['id'] == type_id:
                return type['full_name']

        return ''