
class AuditoryHelper:

    def __init__(self, structure):
        self.structure = structure

    def auditory_id_by_name(self, name):
        for building in self.structure['university']['buildings']:
            for auditory in building['auditories']:
                if auditory['short_name'] == name:
                    return int(auditory['id'])
        return -1
