from datetime import datetime, timedelta


class Time:

    def __init__(self):
        self.current_time = datetime.now()

    def get_time_in_seconds(self):
        return (self.current_time - datetime(1970, 1, 1,)).total_seconds()

    def add_days(self, days_num):
        self.current_time += timedelta(days=days_num)

    def add_weeks(self, week_number):
        self.current_time += timedelta(days=week_number * 7)

    def add_months(self, month_number):
        self.current_time += timedelta(days=month_number * 30)

    @staticmethod
    def get_today_bounds():
        today = datetime.now()

        return ((datetime(today.year, today.month, today.day, 1) - datetime(1970, 1, 1)).total_seconds(),
                 (datetime(today.year, today.month, today.day, 23) - datetime(1970, 1, 1)).total_seconds())