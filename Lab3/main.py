from tasks.task_a import TaskA
from tasks.task_b import TaskB
from tasks.task_c import TaskC
from tasks.task_d import TaskD
from tasks.cist_base_task import CistBaseTask

from utils.time_util import Time
from utils.auditory_helper import AuditoryHelper

cist_base = CistBaseTask()

task_a = TaskA()
#print('------------------- Факультети и кафедри ----------------------------')
#task_a.process_cist_request()

print('------------------- Расписание группы  ----------------------------')

print('enter schedule period')

start = Time()
end = Time()

#months = input('enter months - ')
#end.add_months(int(months))

#weeks = input('enter weeks - ')
#end.add_weeks(int(weeks))

#days = input('enter days - ')
#end.add_days(int(days))

#task_b = TaskB()
#task_b.parse_schedule(start.get_time_in_seconds(), end.get_time_in_seconds())

# print('------------------- Расписание аудитории  ----------------------------')
#
# auditory_name = input("enter auditory name - ")
# university = cist_base.open_json_by_url(cist_base.root_url + "P_API_AUDITORIES_JSON")
#
# helper = AuditoryHelper(university)
# auditory_id = helper.auditory_id_by_name(auditory_name)
# print('auditory id - ' + str(auditory_id))
#
# task_c = TaskC()
# task_c.parse_auditory_schedule(auditory_id)

print('------------------- Расписание всех лекций в университете  ----------------------------')

task_d = TaskD()
task_d.process_lections_schedule()

