from tasks import task1, task3, task4, task5, task6, task7, task8, task9, task10
import random

# task 1
print('--------------------- task 1 -------------------------')

task1.print_rectangle(7, 6)

# task 3
print('--------------------- task 3 -------------------------')

print('composable (5, 3, 2) ')
task3.composable(5, 3, 2)

print('composable (4, 6, 2) ')
task3.composable(4, 6, 2)

print('composable (5, 3, 8) ')
task3.composable(5, 3, 8)

print('composable (5, 13, 2) ')
task3.composable(5, 13, 2)

# task 4
print('--------------------- task 4 -------------------------')

for n in range(2, 14, 1):
    print("square sum of digits from 1 to {0} = {1} ".format(n - 1, task4.square_sum(n)))

# task 5
print('--------------------- task 5 -------------------------')

input_str = ""
while True:
    input_str = input("enter a digit - ");

    if input_str == "skip" :
        break

    print("encoded is {0}".format(task5.encode_characters(int(input_str))))


# task 6
print('--------------------- task 6 -------------------------')

print('generated array of long 30')
print(task6.generate_array(30))

# task 7
print('--------------------- task 7 -------------------------')

list = [ random.randint(1, x) for x in range(101, 111, 1) ]
print('selecting max value in ')
print(list)
print('and received')
print(task7.analyze_array(list))

# task 8
print('--------------------- task 8 -------------------------')

arr = [1, -101, 101, 900, 12321]

print('filtering array')
print(arr)
print('received')
print(task8.filter_array(arr))

# task 9
print('--------------------- task 9 -------------------------')

print('generated matrix for rows = 5, columns = 5')
print(task9.generate_matrix(5, 5))
print('generated matrix for rows = 10, column = 1')
print(task9.generate_matrix(10, 1))

# task 10
print('--------------------- task 10 -------------------------')

input_str = ""
while True:
    input_str = input("enter a string ( 'skip' to continue) - ")

    if input_str == "skip" :
        break

    print("formatted is '{0}'".format(task10.remove_abc(input_str)))
