def encode_characters(n):
    result_string = ""
    while n > 1:
        to_encode = n % 10
        result_string += chr(int(97 + to_encode))
        n /= 10
    return result_string[::-1]
