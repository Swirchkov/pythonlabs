def square_sum(n):
    return sum([x**2 for x in range(0, n, 1)])