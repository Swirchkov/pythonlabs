def get_digits(val):
    list = []

    while val > 1:
        list.append(val % 10)
        val /= 10

    return list

def is_pallindrom(val):

    if val < 10:
        return val >= 0

    digits = get_digits(val)

    i=0
    while i <= len(digits) / 2:

        if digits[i] != digits[-1 - i]:
            return False

    return True

def filter_array(arr):
    print(arr)
    for index, value in enumerate(arr):

        if not( value > 0 and is_pallindrom(value) ):
            print('deleting {0}'.format(value))
            del arr[index]

    return arr