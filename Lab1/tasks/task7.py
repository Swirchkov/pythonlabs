
def analyze_array(list):
    return max([list[x] for x in range(0, len(list)) if list[x] % 3 == 0])
