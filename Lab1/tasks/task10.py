import re

def remove_abc(str):
    return re.sub(r"abc[0-9]", '', str)
