def add_one_to_row(row):
    row[0] = 1
    return row


def generate_matrix(m,n):
    if m <= 0 or n <= 0:
        return

    matrix = [[0 for x in range(n)] for y in range(m)]

    return list(map(add_one_to_row, matrix))
