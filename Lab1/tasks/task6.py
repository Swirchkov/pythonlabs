
def generate_array(n):
    return [1 if x % 2 != 0 else x % 5 for x in range(0, n)]