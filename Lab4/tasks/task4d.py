import psycopg2

select_popular_kinds = 'select kind."KindName", Count(offer."OwnerId")' \
                    'FROM public."Kind" kind JOIN public."Offer" offer ON offer."KindId" = kind."KindId"' \
                    'GROUP BY kind."KindName"' \
                    'HAVING Count(offer."OwnerId") > 1'


def print_popular_kinds():
    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(select_popular_kinds)

        rows = cur.fetchall()

        print("Total number of kinds with more than 1 offer - {0}".format(len(rows)))

        for row in rows:
            print('Kind name - \'{0}\', Offers count - \'{1}\''.format(row[0], row[1]))

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()