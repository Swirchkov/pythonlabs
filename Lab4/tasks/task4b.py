import psycopg2

query = "SELECT \"KindId\", \"KindName\", \"Info\", \"FamilyId\"" \
	"FROM public.\"Kind\" kind WHERE EXISTS ( SELECT * FROM public.\"Offer\" o WHERE o.\"KindId\" = kind.\"KindId\");"

def print_kinds_with_offers():
    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(query)

        rows = cur.fetchall()

        print("Total number of kinds, which are mentioned in orders - {0}".format(len(rows)))

        for row in rows:
            print('Kind id - {0},  kind name - \'{1}\', info - \'{2}\', family id - \'{3}\''.format(row[0], row[1], row[2], row[3]))

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()