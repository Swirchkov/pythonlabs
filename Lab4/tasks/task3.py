
import psycopg2

from models.family import Family

select_all_families = "select * from public.\"Family\""

select_family_by_id = "select * from public.\"Family\" where \"FamilyId\" = %(family_id)s"

insert_new_family = "INSERT INTO public.\"Family\" ( \"FamilyId\", \"FamilyName\", \"Info\" ) " \
                    "VALUES ( %(family_id)s, %(family_name)s, %(info)s )"

update_exiting_family = "UPDATE public.\"Family\" SET \"FamilyName\"=%(family_name)s, \"Info\" = %(info)s" \
                        "WHERE \"FamilyId\" = %(family_id)s"


def add_family(family: Family):

    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(select_family_by_id, { "family_id": family.family_id })

        rows = cur.fetchall()

        if len(rows) > 0:
            cur.execute(update_exiting_family, {
                "family_id": family.family_id,
                "family_name": family.family_name,
                "info": family.info
            })
        else:
            cur.execute(insert_new_family, {
                "family_id": family.family_id,
                "family_name": family.family_name,
                "info": family.info
            })

        conn.commit()
        cur.close()

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()


def print_families():
    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(select_all_families)

        rows = cur.fetchall()

        print("Total number of families - {0}".format(len(rows)))

        for row in rows:
            print('Family name - \'{0}\', info - \'{1}\''.format(row[1], row[2]))

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()