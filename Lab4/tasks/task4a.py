import psycopg2

select_all_families = "select * from public.\"Family\""


def print_all_families():
    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(select_all_families)

        rows = cur.fetchall()

        print("Total number of families - {0}".format(len(rows)))

        for row in rows:
            print('Family id - {0},  family name - \'{1}\', info - \'{2}\''.format(row[0], row[1], row[2]))

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()