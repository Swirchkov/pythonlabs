import psycopg2

select_all_offers = 'SELECT kind."KindName", own."OwnerName", offer."Count", offer."Price" ' \
                    'FROM public."Offer" offer ' \
                    'JOIN public."Owner" own ON own."OwnerId" = offer."OwnerId" ' \
                    'JOIN public."Kind" kind ON kind."KindId" = offer."KindId";'


def print_offers():
    conn = None

    try:
        conn = psycopg2.connect("dbname='plants' host='localhost' user='postgres' password='1111'")

        cur = conn.cursor()

        cur.execute(select_all_offers)

        rows = cur.fetchall()

        print("Total number of offers - {0}".format(len(rows)))

        for row in rows:
            print('Owner name - \'{0}\', Kind name - \'{1}\', Count - \'{2}\' Price - \'{3}\''.format(row[0], row[1], row[2], row[3]))

    except Exception as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()