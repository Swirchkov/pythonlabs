from tasks.task3 import add_family, print_families
from tasks.task4a import print_all_families
from tasks.task4b import print_kinds_with_offers
from tasks.task4c import print_offers
from tasks.task4d import print_popular_kinds

from models.family import Family

# task3
print('-------------------------- task 3 -------------------------------')
family1 = Family(14, "Family from python", "Updated by code")

add_family(family1)
print_families()

# task 4a
print('-------------------------- task 4a -------------------------------')
print_all_families()

# task 4b
print('-------------------------- task 4b -------------------------------')
print_kinds_with_offers()

# task 4c
print('-------------------------- task 4c -------------------------------')
print_offers()

# task 4d
print('-------------------------- task 4d -------------------------------')
print_popular_kinds()
