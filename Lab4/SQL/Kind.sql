CREATE TABLE public."Kind"
(
    "KindId" integer NOT NULL,
    "KindName" character varying(50),
    "Info" character varying(50),
    "FamilyId" integer NOT NULL,
    PRIMARY KEY ("KindId"),
    CONSTRAINT "KInd_Family" FOREIGN KEY ("FamilyId")
        REFERENCES public."Family" ("FamilyId") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public."Kind"
    OWNER to postgres;