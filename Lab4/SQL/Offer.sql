CREATE TABLE public."Offer"
(
    "OwnerId" integer NOT NULL,
    "KindId" integer NOT NULL,
    "Count" integer NOT NULL,
    "Price" integer NOT NULL,
    PRIMARY KEY ("OwnerId", "KindId"),
    CONSTRAINT "Offer_Owner" FOREIGN KEY ("OwnerId")
        REFERENCES public."Owner" ("OwnerId") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "Offer_Kind" FOREIGN KEY ("KindId")
        REFERENCES public."Kind" ("KindId") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public."Offer"
    OWNER to postgres;