INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (1, 'Kind 1', 'kind info 22', 1);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (2, 'Kind 2', 'kind info 22', 1);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (3, 'Kind 3', 'kind info 33', 1);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (4, 'Kind 4', 'kind info 44', 2);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (5, 'Kind 5', 'kind info 55', 2);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (6, 'Kind 6', 'kind info 66', 3);
    
INSERT INTO public."Kind"(
	"KindId", "KindName", "Info", "FamilyId")
	VALUES (7, 'Kind 7', 'kind info 77', 4);