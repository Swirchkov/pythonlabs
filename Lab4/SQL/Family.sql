CREATE TABLE public."Family"
(
    "FamilyId" integer NOT NULL,
    "FamilyName" character varying(50),
    "Info" character varying(50),
    PRIMARY KEY ("FamilyId")
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public."Family"
    OWNER to postgres;