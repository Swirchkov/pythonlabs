CREATE TABLE public."Owner"
(
    "OwnerId" integer NOT NULL,
    "OwnerName" character varying(50),
    "Surname" character varying(50),
    PRIMARY KEY ("OwnerId")
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public."Owner"
    OWNER to postgres;